package me.icyrelic.com.Listeners;

import java.util.Collection;
import java.util.Random;

import me.icyrelic.com.NoDrops;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class disableDrops implements Listener {
	
	NoDrops plugin;
	public disableDrops(NoDrops instance){
		plugin = instance;
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e){
		
		Player p = e.getPlayer();
		Block b = e.getBlock();
		int dropped = e.getExpToDrop();
		Inventory inv = p.getInventory();
		p.giveExp(dropped);
		e.setExpToDrop(0);
		int firstEmpty = inv.firstEmpty();
		Collection<ItemStack> drops = b.getDrops();
		
		
		for(ItemStack item : inv){
			
			if(item != null){
				
				
				for(ItemStack drop : drops){
					
					if(item.getType().equals(drop.getType())){
						if(!(item.getMaxStackSize() >= item.getAmount())){
							int give = 0;
							int given = 0;
							Random rand = new Random();
							if(b.getType().equals(Material.LAPIS_ORE)){
								give = rand.nextInt((8 - 4) + 1) + 4;
							}else if(b.getType().equals(Material.GLOWSTONE)){
								give = rand.nextInt((4 - 2) + 1) + 2;
							}else if(b.getType().equals(Material.REDSTONE_ORE) || b.getType().equals(Material.GLOWING_REDSTONE_ORE)){
								give = rand.nextInt((5 - 4) + 1) + 4;
							}else if(p.getItemInHand().getEnchantments().containsKey(Enchantment.LOOT_BONUS_BLOCKS)){
								
								if(b.getType().equals(Material.COAL_ORE) || b.getType().equals(Material.IRON_ORE) || b.getType().equals(Material.GOLD_ORE) || 
										b.getType().equals(Material.DIAMOND_ORE) || b.getType().equals(Material.LAPIS_ORE) || b.getType().equals(Material.REDSTONE_ORE) || b.getType().equals(Material.GLOWING_REDSTONE_ORE) || 
										b.getType().equals(Material.EMERALD_ORE) || b.getType().equals(Material.STONE)){
									Random r=new Random();
							    	give=(r.nextInt(getMinMax(7, p.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS))) +getMinMax(3, p.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS)));
									
								}
							}else{
								give = 1;
							}
							
							while (give > given){
								inv.addItem(drop);
								given++;
							}
							
							b.setType(Material.AIR);
							return;
						}
					}
					
				}
				
				
			}
		}
		
		
		
		if(firstEmpty >= 0){
			for(ItemStack drop : drops){
				
				int give = 0;
				int given = 0;
				Random rand = new Random();
				if(b.getType().equals(Material.LAPIS_ORE)){
					give = rand.nextInt((8 - 4) + 1) + 4;
				}else if(b.getType().equals(Material.GLOWSTONE)){
					give = rand.nextInt((4 - 2) + 1) + 2;
				}else if(b.getType().equals(Material.REDSTONE_ORE) || b.getType().equals(Material.GLOWING_REDSTONE_ORE)){
					give = rand.nextInt((5 - 4) + 1) + 4;
				}else if(p.getItemInHand().getEnchantments().containsKey(Enchantment.LOOT_BONUS_BLOCKS)){
					if(b.getType().equals(Material.COAL_ORE) || b.getType().equals(Material.IRON_ORE) || b.getType().equals(Material.GOLD_ORE) || 
							b.getType().equals(Material.DIAMOND_ORE) || b.getType().equals(Material.LAPIS_ORE) || b.getType().equals(Material.REDSTONE_ORE) || b.getType().equals(Material.GLOWING_REDSTONE_ORE) || 
							b.getType().equals(Material.EMERALD_ORE) || b.getType().equals(Material.STONE)){
						Random r=new Random();
				    	give=(r.nextInt(getMinMax(7, p.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS))) +getMinMax(3, p.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS)));
						
					}
				}else{
					give = 1;
				}
				
				while (give > given){
					inv.addItem(drop);
					given++;
				}
				
				
				b.setType(Material.AIR);
				return;
			}
			
		}
		
		
	}
	
	

	
	
	public int getMinMax(double x, int enlvl){
		int lvl = 1;
		
		while(lvl != enlvl){
			x=x*1.1;
			lvl++;
		}
		Double d = new Double(x);
		
		return d.intValue();
		
		
	}
}
